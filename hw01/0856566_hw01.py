import numpy as np
import matplotlib.pyplot as plt
import math

def error(x1,x0):
    total=0
    for i in range(x1.shape[0]):
        total = total + math.pow((x1[i] - x0[i]),2)
    return total

def inverse_array(L,U):
    b = np.zeros((L.shape[1], L.shape[1]))
    for i in range(len(L)):
        b[i][i]=1
    y = np.linalg.solve(L, b)
    x = np.linalg.solve(U, y)

    return x

def LU_decomposition(com_arr):
    U= com_arr
    L = np.zeros((com_arr.shape[1], com_arr.shape[1]))

    for i in range(U.shape[0]-1):
        L[i, i] = 1
        L[i+1, i+1] = 1
        for k in range(i , L.shape[0]-1):
            tmp = float(U[k + 1, i] / U[i, i])
            L[k + 1, i] = tmp
            for j in range( i, U.shape[0]):
                U[k+1,j] = float(U[k+1,j] - float((tmp * U[ i, j])))
    return L , U

def LSE(poly_base,reg_lambda,input_array):
    # A x = B
    A_array = np.zeros( (len(input_array), poly_base) )
    B_array = input_array[:,-1]

    for i in range(len(input_array)):
        for j in range(poly_base-1,-1,-1):
            A_array[i][poly_base-1-j] = float((math.pow(input_array[i][0],j)))

    #standard
    standard_array = np.ones((A_array.shape[1],A_array.shape[1]))
    for i in range(len(standard_array)):
        for j in range(len(standard_array)):
            if ( i != j):
                standard_array[i][j] = 0
    # lambda
    lambda_array = reg_lambda * standard_array

    #(At*A+ λI)-1
    ata = np.dot(A_array.T,A_array)
    L , U = LU_decomposition(ata + lambda_array)
    tmp_array = inverse_array(L,U)
    #(At*A+ λI)-1 * At * b
    tmp_array2 = np.dot(tmp_array,A_array.T)
    final_array = np.dot(tmp_array2,B_array)

    return final_array

def Newton(poly_base,input_array):
    #initial
    x0 = np.ones((poly_base,1))
    x1 = np.zeros((poly_base, 1))

    # A x = B
    A_array = np.zeros((len(input_array), poly_base))
    B_array = input_array[:, -1]

    for i in range(len(input_array)):
        for j in range(poly_base - 1, -1, -1):
            A_array[i][poly_base - 1 - j] = float((math.pow(input_array[i][0], j)))

    while error(x1,x0) > 0.0001:
        x0 = x1
        #Hession
        Hession_array = 2 * np.dot(A_array.T,A_array)
        #Hf inverse
        L, U = LU_decomposition(Hession_array)
        inverse_hession = inverse_array(L, U)
        #Gradient
        #Atb
        tmp_array = np.dot(A_array.T,B_array)
        tmp_array = np.reshape(tmp_array,(poly_base,1))
        g_f = 2 * np.dot(np.dot(A_array.T,A_array),x0) - 2 * tmp_array
        #newton
        x1 = x0 - np.dot(inverse_hession,g_f)

    return x1

def line(x_axis,final_array,poly_base):
    lse_tmp = np.zeros((x_axis.shape[0],poly_base))
    for i in range(x_axis.shape[0]):
        for j in range(poly_base-1,-1,-1):
            lse_tmp[i][poly_base-1-j] = float((math.pow(x_axis[i],j)))
    lse_y = np.dot(lse_tmp,final_array)

    return lse_y

def output(lse,newton,input,poly_base):
    A_array = input[:, 0:1]
    B_array = input[:, 1:2]
    lse_error = error(np.reshape(line(A_array,lse,poly_base),(23,1)),B_array)
    newton_error = error(line(A_array, newton, poly_base), B_array)
    #lse_equation
    print("LSE:\n"
           "Fitting line: ")
    for i in range(poly_base - 1, -1, -1):
        if(i!=0):
            print("%fX^%d" % (lse[poly_base-1-i],i),end='')
            if(lse[poly_base-i]>=0):
                print(" + ",end='')
        else:
            print(lse[poly_base-1-i])
    print("Total error: %f\n" % (lse_error))
    #newton_equation
    print("Newton's Method:\n"
          "Fitting line: ")
    for i in range(poly_base - 1, -1, -1):
        if (i != 0):
            print("%fX^%d" % (newton[poly_base - 1 - i], i), end='')
            if (newton[poly_base-i] >= 0):
                print(" + ", end='')
        else:
            print(float(newton[poly_base-1-i]))
    print("Total error: %f" % (newton_error))

def main():
    poly_base = int(input("input n:"))
    reg_lambda = int(input("input lambda:"))
    input_array = np.genfromtxt('testfile.txt','float64', delimiter=',')
    fig, (ax1, ax2) = plt.subplots( 2, 1, sharex=True, sharey=True, figsize=(12, 8))
    ax1.set_xlim(-6, 6)
    ax2.set_xlim(-6, 6)

    LSE_array = LSE(poly_base,reg_lambda,input_array)
    Newton_array = Newton(poly_base,input_array)

    output(LSE_array, Newton_array, input_array, poly_base)

    lse_x = np.arange(-6, 6, 0.01)
    lse_y = line(lse_x,LSE_array,poly_base)
    newton_x = np.arange(-6, 6, 0.01)
    newton_y = line(newton_x, Newton_array, poly_base)

    ax1.plot(lse_x, lse_y)
    ax1.plot(input_array[:,0], input_array[:,1], "ro")
    ax2.plot(newton_x, newton_y)
    ax2.plot(input_array[:, 0], input_array[:, 1], "ro")
    plt.show()

if __name__ == "__main__":
    main()





